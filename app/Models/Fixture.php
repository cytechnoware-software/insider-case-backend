<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Fixture extends Model
{
    public function homeTeam()
    {
        return $this->belongsTo(Team::class, 'home');
    }

    public function awayTeam()
    {
        return $this->belongsTo(Team::class, 'away');
    }

    public function winner()
    {
        if ($this->winning == 'tie') {
            return "Beraberlik";
        }
        if ($this->winning == 'home') {
            return $this->home->name;
        }

        if ($this->winning == 'away') {
            return $this->away->name;
        }
        return "";
    }
}
