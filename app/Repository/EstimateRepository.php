<?php

namespace App\Repository;

use App\Http\Controllers\ApiResponse;
use App\Models\Fixture;
use Exception;

class EstimateRepository
{
    public function weekEstimate($week)
    {
        $teamService = new TeamRepository();
        $teams = $teamService->getTeams();
        $data = [];
        $totalPoint = 0;
        $weekPoint = 0;
        //haftalık performanslar degerlendiriliyor bereabelik 10 puan galibiyet 30 puan alıyor
        foreach ($teams as $team) {
            $totalPoint += $team->point;
            $teamfixture = Fixture::orWhere('home', $team->id)->orWhere('away', $team->id)->get();
            $total = 0;
            foreach ($teamfixture as $item) {
                $add = 0;
                if ($item->winning == "tie") {
                    $add = 10;
                }
                if ($item->winning == "home" and $item->home == $team->id) {
                    $add = 30;
                }
                if ($item->winning == "away" and $item->away == $team->id) {
                    $add = 30;
                }
                $total += $add;
            }

            $data2 = [];
            $data2['name'] = $team->name;
            $data2['point'] = $team->point;
            $data2['week_point'] = $total;
            $data2['total_point'] = 0;
            array_push($data, $data2);
        }
        //Fikstur toplam puan agırlıgı alınıyor
        $i = 0;
        foreach ($data as $item) {
            $pointRatio = $item['point'] / $totalPoint * 100;
            $weekPoint += $pointRatio + $item['week_point'];
            $data[$i]['total_point'] = $pointRatio + $item['week_point'];
            $i++;
        }
        $newData = [];
        foreach ($data as $item) {
            $data2 = [];
            $data2['name'] = $item['name'];
            $data2['estimate'] = number_format($item['total_point'] / $weekPoint * 100, 1);
            array_push($newData, $data2);
        }

        return $newData;
    }
}
