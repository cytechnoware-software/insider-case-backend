<?php

namespace App\Repository;

use App\Models\Team;

class TeamRepository
{
    //Takım Listesini Getirir
    public function getTeams()
    {
        return Team::all();
    }

    //Takım Sayısını döner(Hesaplamalar için)
    public function weekCount()
    {
        return (Team::count() - 1) * 2;
    }

    //Takımları factory kullanarak olusturur
    public function createTeam()
    {
        return factory(Team::class, 4)->create();
    }

    //Moral ve takım desteklerini her hafta ayarlar
    public function setTeamPower()
    {
        $teams = $this->getTeams();
        foreach ($teams as $team) {
            $min = 0;
            $max = 10;
            $team->fan_support = random_int($min, $max);
            $team->morale = random_int($min, $max);
            $team->save();
        }
        return true;
    }

    public function setTeamPoint($id, $goal, $point, $status)
    {
        $team = Team::findOrFail($id);
        if ($status == 'tie') {
            $team->tie_count += 1;
        }
        if ($status == 'winner') {
            $team->win_count += 1;
        }
        if ($status == 'loser') {
            $team->defeat_count += 1;
        }
        $team->average += $goal;
        $team->point += $point;
        $team->save();
    }

}
