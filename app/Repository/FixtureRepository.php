<?php

namespace App\Repository;

use App\Http\Controllers\ApiResponse;
use App\Models\Fixture;
use Exception;

class FixtureRepository
{
    public function createFixture()
    {
        //algoritma  https://github.com/Sami-donmez/90-pixel-lig-uygulamasi/blob/master/src/Service/fiksturOlusturucu.php adresinden alınmıstır
        $teamService = new  TeamRepository();
        $teams = $teamService->getTeams()->pluck('id')->toArray();
        if (is_null($teams) or count($teams) == 0) {
            throw new Exception("Önce Takımları Oluşturunuz");
        }
        $fixture = [];
        $array = $teams;
        $list = $teams;
        $first = array_shift($array);
        for ($i = 0; $i < 3; $i++) {
            array_push($fixture, [$first, $array[0], $i + 1]);
            array_push($fixture, [$array[1], $array[2], $i + 1]);
            $change = array_pop($array);
            array_unshift($array, $change);
        }
        $array = array_reverse($list);
        $first = array_shift($array);
        for ($i = 0; $i < 3; $i++) {
            array_push($fixture, [$first, $array[0], $i + 4]);
            array_push($fixture, [$array[1], $array[2], $i + 4]);
            $change = array_pop($array);
            array_unshift($array, $change);
        }
        foreach ($fixture as $item) {
            $this->create([
                'home' => $item[0],
                'away' => $item[1],
                'week' => $item[2]
            ]);
        }
        return Fixture::all();
    }

    public function createFixtureResponse($fixtures)
    {
        $teamService = new TeamRepository();
        $weekCount = $teamService->weekCount();
        $data = [];
        for ($i = 1; $i < $weekCount + 1; $i++) {
            $data[$i] = ['rounds' => [], 'week' => $i];
        }
        foreach ($fixtures as $fixture) {
            array_push($data[$fixture->week]['rounds'],
                ["home" => $fixture->homeTeam->name,
                    "away" => $fixture->awayTeam->name]);
        }
        return $data;
    }

    public function create($item)
    {
        $fixture = new  Fixture();
        $fixture->home = $item['home'];
        $fixture->away = $item['away'];
        $fixture->week = $item['week'];
        $fixture->home_goal = 0;
        $fixture->away_goal = 0;
        $fixture->winning = null;
        $fixture->save();
    }

    public function weekFixture($week)
    {
        return Fixture::where('week', $week)->get();
    }

    public function weekFixtureResponse($week)
    {
        $fixtures = Fixture::where('week', $week)->get();
        $data = [];
        foreach ($fixtures as $fixture) {
            $data2 = [];
            $data2['home'] = $fixture->homeTeam->name . "(" . $fixture->home_goal . ")";
            $data2['away'] = $fixture->awayTeam->name . "(" . $fixture->away_goal . ")";
            array_push($data, $data2);
        }
        return $data;

    }

}
