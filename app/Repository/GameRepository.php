<?php

namespace App\Repository;

class GameRepository
{
    public function startGame($fixture)
    {
        $home = $fixture->homeTeam;
        $away = $fixture->awayTeam;
        $awayPoint = $away->power;
        $homePoint = $home->power;
        $awayFullPoint = $away->power + $away->fan_support + $away->morale;
        //Evinde oynayan takım +3 puan saha destegi alır
        $homeFullPoint = $home->power + $home->fan_support + $home->morale + 3;
        $chance = random_int(0, 100);
        $tieGoal = random_int(0, 6);
        $loserGoal = random_int(0, 3);
        $winnerGoal = random_int(3, 6);

        // secilen 50-65 aralıgında beraber biter maç
        if (50 <= $chance and $chance <= 65) {
            $winner = 'tie';
        } // seçilen sayı 90 büyükse puanı az olan kazanır
        else if ($chance >= 90) {
            if ($homePoint < $awayPoint) {
                $winner = 'home';
            }

            if ($homePoint >= $awayPoint) {
                $winner = 'away';
            }
        } // Puanlara bakılır
        else {
            if ($homeFullPoint == $awayFullPoint) {
                $winner = 'tie';
            } elseif ($homeFullPoint > $awayFullPoint) {
                $winner = 'home';
            } else {
                $winner = 'away';
            }

        }

        if ($winner == 'tie') {
            $fixture->home_goal = $tieGoal;
            $fixture->away_goal = $tieGoal;
            $homeRoundPoint = 1;
            $awayRoundPoint = 1;
            $homeStatus = "tie";
            $awayStatus = "tie";
        }
        if ($winner == 'home') {
            $fixture->home_goal = $winnerGoal;
            $fixture->away_goal = $loserGoal;
            $homeRoundPoint = 3;
            $awayRoundPoint = 0;
            $homeStatus = "winner";
            $awayStatus = "loser";
        }
        if ($winner == 'away') {
            $fixture->home_goal = $loserGoal;
            $fixture->away_goal = $winnerGoal;
            $homeRoundPoint = 0;
            $awayRoundPoint = 3;
            $homeStatus = "loser";
            $awayStatus = "winner";
        }
        $fixture->winning = $winner;
        $fixture->save();
        $service = new TeamRepository();
        $service->setTeamPoint($fixture->home, $fixture->home_goal, $homeRoundPoint, $homeStatus);
        $service->setTeamPoint($fixture->away, $fixture->away_goal, $awayRoundPoint, $awayStatus);
        return true;

    }
}
