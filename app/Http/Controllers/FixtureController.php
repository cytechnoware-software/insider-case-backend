<?php

namespace App\Http\Controllers;

use App\Http\Resources\FixtureResource;
use App\Http\Resources\TeamResource;
use App\Models\Fixture;
use App\Repository\FixtureRepository;
use App\Repository\TeamRepository;
use Illuminate\Http\Request;

class FixtureController extends Controller
{

    private $service;
    private $teamService;

    public function __construct()
    {
        $this->service = new FixtureRepository();
        $this->teamService = new TeamRepository();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try {

            $fixtures = $this->service->createFixture();
            $fixtures = $this->service->createFixtureResponse($fixtures);

            return ApiResponse::ok([
                'fixture' => $fixtures
            ]);
        } catch (\Exception $exception) {
            return ApiResponse::error(500, $exception->getMessage());
        }
    }
}
