<?php

namespace App\Http\Controllers;

use App\Repository\EstimateRepository;
use Illuminate\Http\Request;

class EstimateController extends Controller
{
    private $service;
    public function __construct()
    {
        $this->service = new EstimateRepository();
    }

    public function estimate($week)
    {
        return ApiResponse::ok($this->service->weekEstimate($week));
    }
}
