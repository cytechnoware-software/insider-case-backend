<?php

namespace App\Http\Controllers;

use App\Http\Resources\TeamResource;
use App\Models\Team;
use App\Repository\TeamRepository;

class TeamController extends Controller
{
    private $service;
    public function  __construct()
    {
        $this->service = new TeamRepository();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $response =$this->service->getTeams();
        return response()->json([
            'teams'=> TeamResource::collection($response)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $response = $this->service->createTeam();
        if (!is_null($response)) {
            return response()->json([
                'success'=>'ok',
                'message'=>'Takımlar oluşturuldu',
                'teams'=> TeamResource::collection($response)
            ]);
        }
    }

}
