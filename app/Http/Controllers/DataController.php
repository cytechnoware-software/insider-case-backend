<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DataController extends Controller
{
    public function reset()
    {
        try {
            DB::statement('SET FOREIGN_KEY_CHECKS = 0;');
            DB::table('fixtures')->truncate();
            DB::table('teams')->truncate();
            DB::statement('SET FOREIGN_KEY_CHECKS = 1;');
            return response()->json([
                'success' => 'ok',
                'message' => 'Veriler Sıfırlandı'
            ]);
        } catch (\Exception $exception) {
            return response()->json([
                'success' => 'error',
                'message' => 'Hata Oluştu',
                'error' => $exception
            ], 500);
        }
    }
}
