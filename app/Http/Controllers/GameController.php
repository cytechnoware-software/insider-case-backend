<?php

namespace App\Http\Controllers;

use App\Repository\EstimateRepository;
use App\Repository\FixtureRepository;
use App\Repository\GameRepository;
use App\Repository\TeamRepository;
use Illuminate\Http\Request;

class GameController extends Controller
{
    private $service;
    private $teamService;
    private $fixtureService;
    private $estimateService;

    public function __construct()
    {
        $this->service = new GameRepository();
        $this->teamService = new TeamRepository();
        $this->fixtureService = new FixtureRepository();
        $this->estimateService = new EstimateRepository();
    }

    public function playWeek($week)
    {
        $this->teamService->setTeamPower();
        $fixtures = $this->fixtureService->weekFixture($week);
        foreach ($fixtures as $fixture) {
            $this->service->startGame($fixture);
        }
        return ApiResponse::ok([
            'message' => $week . '. Hafta Maçları Oynandı.',
            'week' => $week,
            'team' => $this->teamService->getTeams(),
            'round' => $this->fixtureService->weekFixtureResponse($week),
            'estimate' => $this->estimateService->weekEstimate($week)
        ]);
    }

    public function playAll()
    {

    }
}
