<?php

namespace App\Http\Controllers;

use Illuminate\Validation\ValidationException;

class ApiResponse extends Controller
{
    public static function error($code, $message = null)
    {
        return response([
            'status' => 'error',
            'message' => $message,
        ], $code);
    }

    public static function errorValidation($key, $value)
    {
        throw ValidationException::withMessages([
            $key => [$value],
        ]);
    }

    public static function ok($data = [], $statusCode = 200)
    {
        $header = ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'];
        return response()->json([
                'status' => 'ok',
            ] + $data, $statusCode, $header, JSON_UNESCAPED_UNICODE);
    }

}
