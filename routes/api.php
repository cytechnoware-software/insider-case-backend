<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('/create-team','TeamController@create');
Route::post('/create-fixture','FixtureController@create');
Route::get('/fixture','FixtureController@index');
Route::get('/estimates','FixtureController@index');
Route::post('/play-week/{week}','GameController@playWeek');
Route::post('/estimate/{week}','EstimateController@index');
Route::post('/team/{week}','TeamController@index');
Route::post('/play-all','GameController@playAll');
Route::post('/reset-data','DataController@reset');

