<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

$factory->define(\App\Models\Team::class, function (Faker $faker) {
    return [
        'name' => $faker->city() . " FC",
        'power' => random_int(40, 100),
        'fan_support' => random_int(0, 10),
        'morale' => random_int(0, 10),
        'point' => 0,
        'average' => 0,
        'win_count' => 0,
        'defeat_count' => 0,
        'tie_count' => 0,
        'estimate' => 0,
    ];
});
