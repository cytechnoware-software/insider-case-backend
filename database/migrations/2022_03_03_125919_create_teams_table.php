<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTeamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teams', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->tinyInteger('power');
            $table->tinyInteger('fan_support');
            $table->tinyInteger('morale');
            $table->tinyInteger('point');//puan
            $table->tinyInteger('average');//averaj
            $table->tinyInteger('win_count');//galibiyet
            $table->tinyInteger('defeat_count');//maglubiyet
            $table->tinyInteger('tie_count');//beraberlik
            $table->tinyInteger('estimate');//Yüzde
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teams');
    }
}
